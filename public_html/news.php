<?php

$items = array(
    "https://www.topgear.com/car-reviews/bmw/m5-competition" => "The quickest M5 ever is also the comfiest M5 ever. Maybe the most complete super saloon of them all (TopGear)",
    "https://www.motor1.com/news/449708/nissan-safety-shield-added-10-models/" => "Nissan Safety Shield 360 Standard On 10 Models Starting In 2021 (Motor1)",
    "https://www.caranddriver.com/news/a34414919/2021-honda-civic-price/" => "2021 Honda Civic Prices Rise $250–$1050 as Lineup Shifts (CarAndDriver)",
);

$tempContent = "<ul>";

foreach ($items as $item => $item_value) {

    $tempContent .= "<li><a href=\"$item\">$item_value</a></li>";
}
$tempContent .= "</ul>";
