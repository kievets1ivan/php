<?php

$pages = array(
    'signIn' => array(
        'title' => 'Sign in'
    ),
    'signUp' => array(
        'title' => 'Sign up'
    ),
    'home' => array(
        'name' => 'Home',
        'title' => 'Home page',
        'content' => '<h2>Car</h2><p>A car (or automobile) is a wheeled motor vehicle used for transportation. Most definitions of cars say that they run primarily on roads, seat one to eight people, have four tires, and mainly transport people rather than goods.</p>
                        <p>Cars came into global use during the 20th century, and developed economies depend on them. The year 1886 is regarded as the birth year of the modern car when German inventor Karl Benz patented his Benz Patent-Motorwagen. Cars became widely available in the early 20th century. One of the first cars accessible to the masses was the 1908 Model T, an American car manufactured by the Ford Motor Company. Cars were rapidly adopted in the US, where they replaced animal-drawn carriages and carts, but took much longer to be accepted in Western Europe and other parts of the world.</p>
                        <p>Cars have controls for driving, parking, passenger comfort, and a variety of lights. Over the decades, additional features and controls have been added to vehicles, making them progressively more complex, but also more reliable and easier to operate. These include rear-reversing cameras, air conditioning, navigation systems, and in-car entertainment. Most cars in use in the 2010s are propelled by an internal combustion engine, fueled by the combustion of fossil fuels. Electric cars, which were invented early in the history of the car, became commercially available in the 2000s and are predicted to cost less to buy than gasoline cars before 2025. The transition from fossil fuels to electric cars features prominently in most climate change mitigation scenarios.</p>
                        <p>There are costs and benefits to car use. The costs to the individual include acquiring the vehicle, interest payments (if the car is financed), repairs and maintenance, fuel, depreciation, driving time, parking fees, taxes, and insurance. The costs to society include maintaining roads, land use, road congestion, air pollution, public health, healthcare, and disposing of the vehicle at the end of its life. Traffic collisions are the largest cause of injury-related deaths worldwide.</p>
                        <p>The personal benefits include on-demand transportation, mobility, independence, and convenience. The societal benefits include economic benefits, such as job and wealth creation from the automotive industry, transportation provision, societal well-being from leisure and travel opportunities, and revenue generation from the taxes. People\'s ability to move flexibly from place to place has far-reaching implications for the nature of societies. There are around 1 billion cars in use worldwide. The numbers are increasing rapidly, especially in China, India and other newly industrialized countries.</p>',
        'backgrImg' => 'backgrHome'
    ),
    'cars' => array(
        include 'cars.php',
        'name' => 'Cars',
        'title' => 'Cars page',
        'content' => $tempContent,
        'backgrImg' => 'backgrCars'
    ),
    'news' => array(
        include 'news.php',
        'name' => 'News',
        'title' => 'News page',
        'content' => $tempContent,
        'backgrImg' => 'backgrNews'
    ),
    'articles' => array(
        'name' => 'Articles',
        'title' => 'Articles page',
        'content' => '<h2>Driven to Succeed: How an Entrepreneur\'s Love of Cars Led to a Lifetime of Success</h2>
                        <p>Todd "TJ" Johnson has both founded and partnered to build companies in the industries of information technology, commercial construction, retail, hospitality, publishing and information marketing, leading these companies through startup, turnaround and growth modes. He\'s also an active real estate investor with properties all over the world. Although he\'s known in Los Angeles for his collection of exotic cars and lively tequila tastings, family still comes first. I caught up with him the day after he surprised his daughters with a hot air balloon ride and asked him to break down his entrepreneurial journey.</p>
                        <h3>Beginnings</h3>
                        <p>"I’m from Cayce, SC, which is a suburb of Columbia. I grew up in low-income housing to a single mom. My family moved to San Francisco, Calif., when I was 11, after my mom remarried, and we lived there for one-and-a-half years before moving back to SC. My first job was doing yard work -- I would cut grass and rake yards. That work taught me to hustle and be resourceful. If I wanted to make money, I simply had to go find customers. Some days I didn’t find work, but most days I would if I kept knocking on doors. I always had money, which gave me a sense of independence, and working made me feel like I was in control of my destiny. That feeling has never gone away."</p>
                        <h3>Buying his first car ... at 14 years old</h3>
                        <p>"I’ve been interested in cars for as long as I can remember. As a kid, I put together model cars, the small scale models that had seemingly 1,000 pieces. The tips of my fingers would become desensitized from all of the model glue. It took time to become organized, detailed and focused enough to build cars that looked great; it was very difficult to build them perfectly. Fortunately, today I can buy them and have discovered that all hand-built cars will have imperfections, no matter how much you pay for them."</p>
                        <h3>The first signs of success</h3>
                        <p>"I joined the Air Force at 17 and went to college at night to study computer science. While in the military, I started building computers and selling them. At that time, IBM compatible computers were all the rage because you could have a PC that did everything an IBM PC did for a fraction of the price."</p>',
        'backgrImg' => 'backgrArticles'
    ),
    '404' => array(
        'name' => 'Не знайдено',
        'title' => 'Сторінку не знайдено',
        'content' => 'Такої сторінки не існує (404)'
    )
);
