<?php

require_once('pages.php');

if (!isset($_GET['page'])) {
    $_GET['page'] = 'signIn';
} elseif (!isset($pages[$_GET['page']])) {
    header("HTTP/1.1 404 Not Found");
    $_GET['page'] = '404';
}


include 'template.php';
