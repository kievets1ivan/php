<?php

$posImg = array("imgLeft", "imgRight");

$items = array(
    "Nissan Murano" => "<p>The Nissan Murano is a mid-size crossover SUV manufactured and marketed by Nissan since May 2002 as a model for 2003, and now in its third generation — with a convertible variant, the CrossCabriolet, available for the model years of 2011 to 2014.</p>
                        <p>As Nissan's first crossover SUV for the United States and Canada, the Murano was designed at Nissan America in La Jolla, California, and was based on the Nissan FF-L platform shared with the third generation Altima. The single European version of the Murano began sales in 2004.</p>
                        <p>The Murano was Nissan's only crossover SUV in the United States until September 2007, when the 2008 Rogue went on sale. In Canada the X-Trail had been on sale as Nissan's second car based SUV since 2004 as a model for 2005; it was replaced by the 2008 Rogue at the end of 2007. The Murano is sized between the Pathfinder and the now defunct Xterra (replaced by the Rogue as a compact SUV).</p>
                        <p>The nameplate Murano derives from the Italian city of Murano and the namesake Murano art glass for which the city is widely known.</p>",
    "BMW X6" => "<p>The BMW X6 is a mid-size luxury crossover by German automaker BMW.</p>
                <p>The first generation (E71) was released for sale in April 2008 for the 2008 model year. The X6 was marketed as a sports activity coupé (SAC) by BMW. It combines the attributes of an SUV (high ground clearance, all-wheel drive and all-weather ability, large wheels and tires) with the stance of a coupé (styling featuring a sloping roof). It was based on the previous generation BMW 5 and 6 Series. E71 development began in 2003 under Peter Tuennermann, after start of E70 X5 development in 2001. Design work by E70 X5 designer Pierre Leclercq was frozen in 2005, with test mules being run from the summer of 2005 and prototypes being tested from late 2006. Production began on December 3, 2007.</p>
                <p>The second generation X6 (F16) was launched at the Paris Motor Show in 2014.</p>",
    "Audi A8" => "<p>The Audi A8 is a four-door, full-size, luxury sedan manufactured and marketed by the German automaker Audi since 1994. Succeeding the Audi V8, and now in its fourth generation, the A8 has been offered with both front- or permanent all-wheel drive—and in short- and long-wheelbase variants. The first two generations employed the Volkswagen Group D platform, with the current generation deriving from the MLB platform. After the original model's 1994 release, Audi released the second generation in late 2002, the third in late 2009, and the fourth and current iteration in 2017.</p>
                <p>Notable for being the first mass-market car with an aluminium chassis, all A8 models have used this construction method co-developed with Alcoa and marketed as the Audi Space Frame.</p>
                <p>A mechanically-upgraded, high-performance version of the A8 debuted in 1996 as the Audi S8. Produced exclusively at Audi's Neckarsulm plant, the S8 is fitted standard with Audi's quattro all-wheel drive system, and is available in short- and long-wheelbase form, the latter coming in 2020.</p>",
    "Ford Focus" => "<p>The Ford Focus is a compact car (C-segment in Europe) manufactured by the Ford Motor Company and created under Alexander Trotman's Ford 2000 plan, which aimed to globalize model development and sell one compact vehicle worldwide. The original Focus was primarily designed by Ford of Europe's German and British teams.</p>
                <p>The Focus was released in July 1998 in Europe, succeeding the Ford Escort, and replaced the Mazda Familia-derived Ford Laser in Asia and Oceania along with the Laser-based North American Escort. Wayne Stamping & Assembly started producing the Focus for North America with sales beginning in 1999. The last North American-produced Focus rolled off the line at the Michigan Assembly Plant on May 4, 2018. Production of a fourth generation Focus began elsewhere in 2018.</p>"
);


$tempContent = "";
$numberImg = 0;
$numberPosImg = 0;
foreach ($items as $item => $item_value) {
    $tempContent .= "<img class=\"$posImg[$numberPosImg]\" src=\"img/$numberImg.jpg\"><h3>$item</h3><p>$item_value</p>";

    $numberImg++;

    if ($numberImg % 2 == 0) {
        $numberPosImg = 0;
    } else {
        $numberPosImg = 1;
    }
}
