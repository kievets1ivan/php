<?php
session_start();
$home = $_SERVER['DOCUMENT_ROOT'] . "/";
$pathToUsers = $home . 'LR1/public_html/users.dat';
$users = file_get_contents($pathToUsers);
$lines = explode("\n", $users);


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $login = trim($_POST['login']);
    $password = trim($_POST['pass']);

    if ((@strpos($users, $login) == TRUE) or (@strpos($users, $password) == TRUE)) {

        for ($i = 0; $i < count($lines); $i++) {
            $parts = explode("::", $lines[$i]);
            if ((trim($parts[0] == $login) and (trim($parts[1] == $password)))) {
                $_SESSION['user'] = $login;
                if (!isset($_COOKIE['user'])) {
                    setcookie("user", $login, time() + 3125);
                }
            }
        }
    }
    if (!empty($_COOKIE['user'])) {
        header('Location: ' . 'http://localhost/LR1/public_html/?page=home');
    } else {
        $response = "Login or password is invalid";
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $pages[$_GET['page']]['title']; ?></title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript">
        function submitform() {
            document.forms["authForm"].submit();
        }
    </script>
</head>

<body>

    <?php if (empty($_SESSION['user'])) { ?>

        <div class="login-box">
            <h2><?php echo $pages[$_GET['page']]['title']; ?></h2>
            <form id="authForm" method="POST">
                <div class="user-box">
                    <input type="text" name="login" required="">
                    <label>Login</label>
                </div>
                <div class="user-box">
                    <input type="password" name="pass" required="">
                    <label>Password</label>
                </div>
                <?php if (!empty($response)) { ?>

                    <div class="error"><?php echo $response ?></div>

                <?php } ?>


                <div class="btn-submit">
                    <a href="javascript: submitform()">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        Sign in
                    </a>
                </div>
            </form>
        </div>

    <?php } else { ?>

        <div id="wrapper">
            <div id="banner">

            </div>

            <div id="navigation">
                <ul id="nav">

                    <?php foreach ($pages as $href => $page) { ?>
                        <?php if ($href != '404' & $href != 'signIn' & $href != 'signUp') { ?>
                            <li><a href="?page=<?php echo $href; ?>">
                                    <?php echo $page['name']; ?>
                                </a></li>
                        <?php } ?>
                    <?php } ?>

                </ul>
            </div>

            <div id="content_area">
                <?php echo $pages[$_GET['page']]['content']; ?>

            </div>

            <div id="sidebar" class="<?php echo $pages[$_GET['page']]['backgrImg']; ?>">

            </div>

            <footer>
                <p>All rights reserved</p>
            </footer>
        </div>

    <?php } ?>

</body>

</html>